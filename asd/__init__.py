# -*- coding: UTF-8 -*-
import os
from pyramid.config import Configurator
from asd.resources import UsersResource, PersonUser, PersonStorage
from asd.views import UsersView


def rest_factory(request):
    return {
        'users': UsersResource(request)
    }


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    abs_root_path = prepare_root_folder(settings.get('root_folder'))
    if abs_root_path:
        settings['abs_root_path'] = abs_root_path
    else:
        raise KeyError(u'Некорректное значение ключа "root_folder": {0}'.format(settings.get('root_folder')))

    config = Configurator(settings=settings)
    config.add_route('rest_api', '/*traverse', factory=rest_factory)
    config.add_view(UsersView, route_name='rest_api')
    config.scan()
    return config.make_wsgi_app()


def prepare_root_folder(folder_name):
    """
    Подготавливает рабочий каталог, если он отсутствует, создает его в домашней директории
    :param folder_name: имя каталога
    :return: абсолютный путь до каталога, либо None
    """
    if folder_name:
        full_path = os.path.join(os.path.expanduser('~'), folder_name)
        if not os.path.exists(full_path):
            os.makedirs(full_path)

        return full_path
