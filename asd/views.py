# -*- coding: UTF-8 -*-
import json
from pyramid.response import Response
from pyramid.view import view_config, view_defaults, notfound_view_config
from asd.resources import UsersResource, PersonStorage, FileObject


@view_defaults(route_name='rest_api', context=UsersResource)
class UsersView(object):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    @view_config(request_method='GET')
    def get(self):
        return Response(
           body=json.dumps(self.context.get_representation()),
           status=self.context.status,
           content_type='application/json; charset=UTF-8'
        )

    @view_config(request_method='POST')
    def post(self):
        r = self.context.create_user(self.request.json_body)
        return Response(
            body=json.dumps(r.get_representation()),
            status=self.context.status,
            content_type='application/json; charset=UTF-8'
        )

    @view_config(request_method='DELETE')
    def delete(self):
        self.context.delete_user()
        return Response(
            status=self.context.status,
            content_type='application/json; charset=UTF-8'
        )


@view_defaults(route_name='rest_api', context=PersonStorage)
class StorageView(object):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    @view_config(request_method='GET')
    def get(self):
        return Response(
           body=json.dumps(self.context.get_representation()),
           status=self.context.status,
           content_type='application/json; charset=UTF-8'
        )

    @view_config(request_method='POST')
    def post(self):
        r = self.context.create_file(self.request.json_body)
        return Response(
            body=json.dumps(r),
            status=self.context.status,
            content_type='application/json; charset=UTF-8'
        )


@view_defaults(route_name='rest_api', context=FileObject)
class FileView(object):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    @view_config(request_method='GET')
    def get(self):
        # TODO: очень коряво, желательно переделать
        if self.context.type == 'content':
            return Response(
                body=self.context.read_file(),
                status=self.context.status,
                content_type='application/octet-stream',
                content_disposition='attachment; filename="{0}"'.format(self.context.file_name)
            )

        return Response(
            body=json.dumps(self.context.get_representation()),
            status=self.context.status,
            content_type='application/json; charset=UTF-8'
        )

    @view_config(request_method='DELETE')
    def delete(self):
        self.context.delete_file()

        return Response(
            status=self.context.status,
            content_type='application/json; charset=UTF-8'
        )

    @view_config(request_method='PUT')
    def put(self):
        self.context.put_in_file(self.request)
        return Response(
            status=self.context.status,
            content_type='application/json; charset=UTF-8'
        )


@notfound_view_config()
def notfound(request):
    return Response(
        body=json.dumps({'message': u'Не найден ресурс с указанным идентификатором'}),
        status='404 Not Found',
        content_type='application/json; charset=UTF-8'
    )
