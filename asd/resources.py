# -*- coding: UTF-8 -*-
import os
import shutil
from pyramid.threadlocal import get_current_registry

DOMAIN_NAME = 'http://domain.com'


class UsersResource(object):
    """
    Класс для работы с пользователями
    """
    def __init__(self, request):
        settings = request.registry.settings
        self._root_folder = settings.get('abs_root_path')
        self.users = self.__get_users(self._root_folder)
        self.status = '200'
        global DOMAIN_NAME  # понимаю, что так делать плохо,а DOMAIN_NAME нужно брать из запроса,
        DOMAIN_NAME = request.application_url  # поставил костыль, не хотелось request всюду пробрасывать

    def __getitem__(self, user_id):
        if user_id in self.users:
            return PersonUser(user_id)
        else:
            raise KeyError

    def get_representation(self):

        return {
            'link': '{0}/users'.format(DOMAIN_NAME),
            'embedded': {
                'users': [{
                            'user_id': user_id,
                            'link': '{0}/users/{1}'.format(DOMAIN_NAME, user_id)
                        } for user_id in self.users]
                }
            }

    def create_user(self, body):
        """
        Создает пользователя, создавая для него каталог
        :param body: тело запроса на создание
        :return: вновь созданный пользователь или None
        """
        # TODO: проверить параметры, переданные в запросе на правильность
        user_id = body.get('user_id')

        # TODO: если пользователь уже существует?
        if user_id and user_id not in self.users:
            self.status = '201'
            self.__create_user_folder(os.path.join(self._root_folder, user_id))
            return PersonUser(user_id)

    @staticmethod
    def __get_users(root_folder):
        """
        Строит список пользовтелей по каталогам из директории хранилища
        :param root_folder: адрес хранилища
        :return: список имен директорий в хранилище
        """
        return [d for d in os.listdir(root_folder)
                if not d.startswith('.') and os.path.isdir(os.path.join(root_folder, d))]

    @staticmethod
    def __create_user_folder(full_path):
        """
        Создает директорию, если она еще не существует
        :param full_path: путь к нужной директории
        :return: None
        """
        if not os.path.exists(full_path):
            os.makedirs(full_path)


class PersonUser(UsersResource):
    """
    Класс для работы с пользователем
    """

    def __init__(self, user_id):
        settings = get_current_registry().settings
        self._root_folder = settings.get('abs_root_path')
        self.id = user_id
        self.status = '200'

    def __getitem__(self, key):
        if key == 'files':
            return PersonStorage(self.id, os.path.join(self._root_folder, self.id))
        else:
            raise KeyError

    def get_representation(self):
        return {
            'user_id': self.id,
            'link': '{0}/users/{1}'.format(DOMAIN_NAME, self.id)
        }

    def delete_user(self):
        """
        Удаляет пользователя вместе с его файлами
        :return: None
        """
        user_id = self.id

        if user_id:
            self.status = '204'
            self.__erase_user_folder(os.path.join(self._root_folder, user_id))

    @staticmethod
    def __erase_user_folder(full_path):
        """
        Удаляет директорию вместе со всем содержимым
        :param full_path: полный путь к папке
        :return: None
        """
        if os.path.exists(full_path):
            shutil.rmtree(full_path)


class PersonStorage(object):
    """
    Хранилище пользователя
    """
    def __init__(self, user_id=None, path=None):
        self.path = path
        self.user_id = user_id
        self.status = '200'

    def __getitem__(self, key):
        if key == 'files':
            return self
        else:
            return FileObject(key, self.path, self.user_id)

    def get_representation(self):
        return {
            'type': 'dir',
            'name': '',
            'link': '{0}/users/{1}/files'.format(DOMAIN_NAME, self.user_id),
            'embedded': {
                'children': self.get_children()
            }
        }

    def get_children(self):
        """
        Возвращает описание содержимого хранилища пользователя
        :return: список словарей с информацией о файлах
        """
        return [FileObject(f_name, self.path, self.user_id).get_representation() for f_name in os.listdir(self.path)]

    def create_file(self, body):
        """
        Создает пустой файл в хранилище пользователя
        :param body: тело запроса
        :return:
        """
        # TODO: обработать недопустимое значение имени файла
        if body.get('type') == 'file':
            self.status = '201'
            new_file = FileObject(body.get('name'), self.path, self.user_id)
            return new_file.create()


class FileObject(dict):
    def __init__(self, file_name, path, user_id):
        self.file_name = file_name
        self.path = path
        self.user_id = user_id
        self._full_path = os.path.join(self.path, self.file_name)
        self.status = '200'
        self.type = 'info'

    def __getitem__(self, key):
        if key == 'content':
            self.type = 'content'
            return self
        else:
            raise KeyError

    def get_representation(self):
        link = '{0}/users/{1}/files/{2}'.format(DOMAIN_NAME, self.user_id, self.file_name)
        return {
            'type': 'file',
            'name': self.file_name,
            'size': os.path.getsize(self._full_path),
            'link': link,
            'content_link': '{0}/content'.format(link)
        }

    def create(self):
        """
        Создает файл
        :return: представление созданного файла
        """
        if not os.path.exists(self._full_path):
            os.mknod(self._full_path)
            return self.get_representation()

    def read_file(self):
        if os.path.isfile(self._full_path):
            with open(self._full_path, 'rb') as f:
                s = f.read()
            return s
        else:
            pass  # TODO: невозможно прочитать файл

    def delete_file(self):
        """
        Удаляет файл
        :return: None
        """
        if os.path.isfile(self._full_path):
            self.status = '204'
            try:
                os.remove(self._full_path)
            except OSError as e:
                self.status = '500'
                print('Error: {0} - {1}.'.format(e.filename, e.strerror))

    def put_in_file(self, request):
        """
        Заменить содержимое файла на новое.
        :param request: файл
        :return: None
        """
        # А нужно ли проверять, что файл есть или создавать в любом случае?
        self.status = '404'
        if os.path.isfile(self._full_path):
            with open(self._full_path, 'wb') as f:
                f.write(request.body)
            self.status = '204'
        else:
            pass  # TODO: нет такого файла


class Error(object):
    """
    Класс для отображения ошибок
    """
    def __init__(self, request, status, msg):
        self.status = status
        self.msg = msg

    def get_representation(self):

        return {
                'message': self.msg
            }